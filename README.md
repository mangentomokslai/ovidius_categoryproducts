# README #

This module illustrates how to prepend block to another block via core_block_abstract_to_html_after event.

Event allows to retrieve any block's html output and modify it.

Module's block ovidius_categoryproducts/categoryproducts is prepended to catalog/category_view block.

Such "layout modification" allows module to be theme independent, because no theme specific template modification is needed in order for module's block to appear on page.

Resources:

http://www.blog.magepsycho.com/usage-of-magento-event-core_block_abstract_to_html_after/

UPDATE: 2017-03-07

 + Added translations
