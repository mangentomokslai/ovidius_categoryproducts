<?php
class Ovidius_Categoryproducts_Model_Observer
{
    public function coreBlockAbstractToHtmlAfter($observer)
    {
        $block = $observer->getBlock();

        /* @var $block Mage_Core_Block_Abstract */
        $block = $observer->getBlock();
        if (get_class($block) ==  'Mage_Catalog_Block_Category_View') {
            $transport = $observer->getTransport();

            $newBlock = Mage::app()->getLayout()->createBlock(
                'ovidius_categoryproducts/categoryproducts',
                'categoryproducts',
                array('template' => 'ovidius_categoryproducts/categoryproducts.phtml')
            );

            $html = $newBlock->renderView() . $transport->getHtml();
            $transport->setHtml($html);
        }

    }
}
