<?php
class Ovidius_Categoryproducts_Block_Categoryproducts extends Mage_Catalog_Block_Product_Abstract
{

    public function getBestsellers()
    {
        $limit = 6;
        $category = Mage::registry('current_category');
        $categoryId = $category->getId();
        $storeId = Mage::app()->getStore()->getId();

        $childCategories = $category->getChildren();
        $categories = array();

        if ($childCategories != "")
            $categories = explode(',', $childCategories);   // include child categories

        array_push($categories, $categoryId);   // append parent category ID

        try {
            $collection = Mage::getResourceModel('reports/product_collection')
                ->addOrderedQty()
                ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id')
                ->addFieldToFilter('category_id', $categories)
                ->setStoreId($storeId)
                ->addStoreFilter($storeId)
                ->setOrder('ordered_qty', 'desc')
                ->addAttributeToSelect(array('name', 'price', 'thumbnail'));

            // filter only in stock products
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $collection->setPage(1, $limit);
        } catch(Exception $e) {
            Mage::logException($e);         // Print exception to exception for administrators
        }
        return $collection;
    }

    public function getNewestProducts()
    {
        $limit = 6;
        $category = Mage::registry('current_category');
        $categoryId = $category->getId();
        $storeId = Mage::app()->getStore()->getId();

        $childCategories = $category->getChildren();
        $categories = array();

        if ($childCategories != "")
            $categories = explode(',', $childCategories);   // include child categories

        array_push($categories, $categoryId);   // append parent category ID

        // Filter products added in last 14 days
        $time = time();
        $to = date('Y-m-d H:i:s', $time);
        $lastTime = $time - 1209600; // 60*60*24*14
        $from = date('Y-m-d H:i:s', $lastTime);

        try {
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect(array('name', 'price', 'thumbnail'))
                ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                ->addFieldToFilter('category_id', $categories)
    			->addStoreFilter($storeId)
                ->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))
                ->setOrder('created_at', 'desc');

            // filter only in stock products
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

            $collection->setPage(1, $limit);
        } catch(Exception $e) {
            Mage::logException($e);
        }
        return $collection;
    }
}
